
`timescale 1 ns / 1 ps

	module mycustomled_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
        output wire [3:0] o_LED,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
// Instantiation of Axi Bus Interface S00_AXI

AXI_WRITE_LOGIC #(

.C_S00_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
.C_S00_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
) SLV (
.i_ACLK(s00_axi_aclk),
.i_ARSTN(s00_axi_aresetn),
.i_AWADDR(s00_axi_awaddr),
.i_AWVALID(s00_axi_awvalid),
.o_AWREADY(s00_axi_awready),
.i_WDATA(s00_axi_wdata),
.i_WVALID(s00_axi_wvalid),
.o_WREADY(s00_axi_wready),
.i_BREADY(s00_axi_bready),
.o_BVALID(s00_axi_bvalid),
.o_BRESP(s00_axi_bresp),
.i_ARADDR(s00_axi_araddr),
.i_ARVALID(s00_axi_arvalid),
.o_ARREADY(s00_axi_arready),
.o_RDATA(s00_axi_rdata),
.o_RRESP(s00_axi_rresp),
.o_RVALID(s00_axi_rvalid),
.i_RREADY(s00_axi_rready),
.o_LED(o_LED)
);

	// Add user logic here

	// User logic ends

	endmodule
