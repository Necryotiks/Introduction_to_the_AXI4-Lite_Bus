

/***************************** Include Files *******************************/
#include "LED_CONTROLLER.h"

/************************** Function Definitions ***************************/
void enableLED(uint32_t value)
{
*(LED_BASE_ADDR+0) = value;
}
void disableLED(uint32_t value)
{
*(LED_BASE_ADDR+0) ^= value;
}
void writeLED(uint32_t value)
{
*(LED_BASE_ADDR+1) = value;
}
uint32_t readLED(uint32_t led_Select)
{
return *LED_BASE_ADDR & led_Select;
}