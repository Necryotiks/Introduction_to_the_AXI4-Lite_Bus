# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# XDC: new/BasicAXI.xdc

# Block Designs: bd/Basic_AXI_Design/Basic_AXI_Design.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design || ORIG_REF_NAME==Basic_AXI_Design} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_LED_CONTROLLER_0_0/Basic_AXI_Design_LED_CONTROLLER_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_LED_CONTROLLER_0_0 || ORIG_REF_NAME==Basic_AXI_Design_LED_CONTROLLER_0_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0/Basic_AXI_Design_processing_system7_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_processing_system7_0_0 || ORIG_REF_NAME==Basic_AXI_Design_processing_system7_0_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_ps7_0_axi_periph_0/Basic_AXI_Design_ps7_0_axi_periph_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_ps7_0_axi_periph_0 || ORIG_REF_NAME==Basic_AXI_Design_ps7_0_axi_periph_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_rst_ps7_0_50M_0/Basic_AXI_Design_rst_ps7_0_50M_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_rst_ps7_0_50M_0 || ORIG_REF_NAME==Basic_AXI_Design_rst_ps7_0_50M_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_xlconstant_0_0/Basic_AXI_Design_xlconstant_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_xlconstant_0_0 || ORIG_REF_NAME==Basic_AXI_Design_xlconstant_0_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_xlconstant_1_0/Basic_AXI_Design_xlconstant_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_xlconstant_1_0 || ORIG_REF_NAME==Basic_AXI_Design_xlconstant_1_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/Basic_AXI_Design_system_ila_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_system_ila_0_0 || ORIG_REF_NAME==Basic_AXI_Design_system_ila_0_0} -quiet] -quiet

# Block Designs: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/bd_c509.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509 || ORIG_REF_NAME==bd_c509} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_0/bd_c509_ila_lib_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_ila_lib_0 || ORIG_REF_NAME==bd_c509_ila_lib_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_1/bd_c509_g_inst_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_g_inst_0 || ORIG_REF_NAME==bd_c509_g_inst_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_2/bd_c509_slot_0_aw_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_slot_0_aw_0 || ORIG_REF_NAME==bd_c509_slot_0_aw_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_3/bd_c509_slot_0_w_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_slot_0_w_0 || ORIG_REF_NAME==bd_c509_slot_0_w_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_4/bd_c509_slot_0_b_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_slot_0_b_0 || ORIG_REF_NAME==bd_c509_slot_0_b_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_5/bd_c509_slot_0_ar_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_slot_0_ar_0 || ORIG_REF_NAME==bd_c509_slot_0_ar_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_6/bd_c509_slot_0_r_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==bd_c509_slot_0_r_0 || ORIG_REF_NAME==bd_c509_slot_0_r_0} -quiet] -quiet

# IP: bd/Basic_AXI_Design/ip/Basic_AXI_Design_auto_pc_0/Basic_AXI_Design_auto_pc_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_auto_pc_0 || ORIG_REF_NAME==Basic_AXI_Design_auto_pc_0} -quiet] -quiet

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0/Basic_AXI_Design_processing_system7_0_0.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_processing_system7_0_0 || ORIG_REF_NAME==Basic_AXI_Design_processing_system7_0_0} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_rst_ps7_0_50M_0/Basic_AXI_Design_rst_ps7_0_50M_0_board.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_rst_ps7_0_50M_0 || ORIG_REF_NAME==Basic_AXI_Design_rst_ps7_0_50M_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_rst_ps7_0_50M_0/Basic_AXI_Design_rst_ps7_0_50M_0.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==Basic_AXI_Design_rst_ps7_0_50M_0 || ORIG_REF_NAME==Basic_AXI_Design_rst_ps7_0_50M_0} -quiet] {/U0 } ]/U0 ] -quiet] -quiet

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_0/ila_v6_2/constraints/ila_impl.xdc
set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==bd_c509_ila_lib_0 || ORIG_REF_NAME==bd_c509_ila_lib_0} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_0/ila_v6_2/constraints/ila.xdc
#dup# set_property DONT_TOUCH TRUE [get_cells [split [join [get_cells -hier -filter {REF_NAME==bd_c509_ila_lib_0 || ORIG_REF_NAME==bd_c509_ila_lib_0} -quiet] {/inst } ]/inst ] -quiet] -quiet

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_0/bd_c509_ila_lib_0_ooc.xdc

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/bd_c509_ooc.xdc

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/Basic_AXI_Design_system_ila_0_0_ooc.xdc

# XDC: bd/Basic_AXI_Design/ip/Basic_AXI_Design_auto_pc_0/Basic_AXI_Design_auto_pc_0_ooc.xdc

# XDC: bd/Basic_AXI_Design/Basic_AXI_Design_ooc.xdc
