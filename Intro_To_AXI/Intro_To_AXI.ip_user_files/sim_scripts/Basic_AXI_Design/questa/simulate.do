onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib Basic_AXI_Design_opt

do {wave.do}

view wave
view structure
view signals

do {Basic_AXI_Design.udo}

run -all

quit -force
