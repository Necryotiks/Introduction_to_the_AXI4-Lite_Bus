-makelib xcelium_lib/xilinx_vip -sv \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
  "/opt/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib -sv \
  "/opt/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/opt/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "/opt/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ipshared/148f/hdl/LED_CONTROLLER_v1_0_S00_AXI.v" \
  "../../../bd/Basic_AXI_Design/ipshared/148f/hdl/LED_CONTROLLER_v1_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_LED_CONTROLLER_0_0/sim/Basic_AXI_Design_LED_CONTROLLER_0_0.v" \
-endlib
-makelib xcelium_lib/axi_infrastructure_v1_1_0 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_vip_v1_1_5 -sv \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/d4a8/hdl/axi_vip_v1_1_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/processing_system7_vip_v1_0_7 -sv \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0/sim/Basic_AXI_Design_processing_system7_0_0.v" \
-endlib
-makelib xcelium_lib/lib_cdc_v1_0_2 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/proc_sys_reset_v5_0_13 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_rst_ps7_0_50M_0/sim/Basic_AXI_Design_rst_ps7_0_50M_0.vhd" \
-endlib
-makelib xcelium_lib/xlconstant_v1_1_6 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/66e7/hdl/xlconstant_v1_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_xlconstant_0_0/sim/Basic_AXI_Design_xlconstant_0_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_xlconstant_1_0/sim/Basic_AXI_Design_xlconstant_1_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/sim/bd_c509.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_0/sim/bd_c509_ila_lib_0.v" \
-endlib
-makelib xcelium_lib/gigantic_mux \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/d322/hdl/gigantic_mux_v1_0_cntr.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_1/bd_c509_g_inst_0_gigantic_mux.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_1/sim/bd_c509_g_inst_0.v" \
-endlib
-makelib xcelium_lib/xlconcat_v2_1_3 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_2/sim/bd_c509_slot_0_aw_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_3/sim/bd_c509_slot_0_w_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_4/sim/bd_c509_slot_0_b_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_5/sim/bd_c509_slot_0_ar_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_6/sim/bd_c509_slot_0_r_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/sim/Basic_AXI_Design_system_ila_0_0.v" \
  "../../../bd/Basic_AXI_Design/ipshared/4718/src/AXI_WRITE_LOGIC.v" \
  "../../../bd/Basic_AXI_Design/ipshared/4718/hdl/mycustomled_v1_0.v" \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_mycustomled_0_0/sim/Basic_AXI_Design_mycustomled_0_0.v" \
-endlib
-makelib xcelium_lib/generic_baseblocks_v2_1_0 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_register_slice_v2_1_19 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_4 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_4 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_4 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \
-endlib
-makelib xcelium_lib/axi_data_fifo_v2_1_18 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_crossbar_v2_1_20 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ace7/hdl/axi_crossbar_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_xbar_0/sim/Basic_AXI_Design_xbar_0.v" \
-endlib
-makelib xcelium_lib/axi_protocol_converter_v2_1_19 \
  "../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_auto_pc_0/sim/Basic_AXI_Design_auto_pc_0.v" \
  "../../../bd/Basic_AXI_Design/sim/Basic_AXI_Design.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

