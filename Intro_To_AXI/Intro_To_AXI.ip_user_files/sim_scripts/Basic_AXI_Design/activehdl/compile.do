vlib work
vlib activehdl

vlib activehdl/xilinx_vip
vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/axi_infrastructure_v1_1_0
vlib activehdl/axi_vip_v1_1_5
vlib activehdl/processing_system7_vip_v1_0_7
vlib activehdl/lib_cdc_v1_0_2
vlib activehdl/proc_sys_reset_v5_0_13
vlib activehdl/xlconstant_v1_1_6
vlib activehdl/gigantic_mux
vlib activehdl/xlconcat_v2_1_3
vlib activehdl/generic_baseblocks_v2_1_0
vlib activehdl/axi_register_slice_v2_1_19
vlib activehdl/fifo_generator_v13_2_4
vlib activehdl/axi_data_fifo_v2_1_18
vlib activehdl/axi_crossbar_v2_1_20
vlib activehdl/axi_protocol_converter_v2_1_19

vmap xilinx_vip activehdl/xilinx_vip
vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap axi_infrastructure_v1_1_0 activehdl/axi_infrastructure_v1_1_0
vmap axi_vip_v1_1_5 activehdl/axi_vip_v1_1_5
vmap processing_system7_vip_v1_0_7 activehdl/processing_system7_vip_v1_0_7
vmap lib_cdc_v1_0_2 activehdl/lib_cdc_v1_0_2
vmap proc_sys_reset_v5_0_13 activehdl/proc_sys_reset_v5_0_13
vmap xlconstant_v1_1_6 activehdl/xlconstant_v1_1_6
vmap gigantic_mux activehdl/gigantic_mux
vmap xlconcat_v2_1_3 activehdl/xlconcat_v2_1_3
vmap generic_baseblocks_v2_1_0 activehdl/generic_baseblocks_v2_1_0
vmap axi_register_slice_v2_1_19 activehdl/axi_register_slice_v2_1_19
vmap fifo_generator_v13_2_4 activehdl/fifo_generator_v13_2_4
vmap axi_data_fifo_v2_1_18 activehdl/axi_data_fifo_v2_1_18
vmap axi_crossbar_v2_1_20 activehdl/axi_crossbar_v2_1_20
vmap axi_protocol_converter_v2_1_19 activehdl/axi_protocol_converter_v2_1_19

vlog -work xilinx_vip  -sv2k12 "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"/opt/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"/opt/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/opt/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ipshared/148f/hdl/LED_CONTROLLER_v1_0_S00_AXI.v" \
"../../../bd/Basic_AXI_Design/ipshared/148f/hdl/LED_CONTROLLER_v1_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_LED_CONTROLLER_0_0/sim/Basic_AXI_Design_LED_CONTROLLER_0_0.v" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_vip_v1_1_5  -sv2k12 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/d4a8/hdl/axi_vip_v1_1_vl_rfs.sv" \

vlog -work processing_system7_vip_v1_0_7  -sv2k12 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0/sim/Basic_AXI_Design_processing_system7_0_0.v" \

vcom -work lib_cdc_v1_0_2 -93 \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work proc_sys_reset_v5_0_13 -93 \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_rst_ps7_0_50M_0/sim/Basic_AXI_Design_rst_ps7_0_50M_0.vhd" \

vlog -work xlconstant_v1_1_6  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/66e7/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_xlconstant_0_0/sim/Basic_AXI_Design_xlconstant_0_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_xlconstant_1_0/sim/Basic_AXI_Design_xlconstant_1_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/sim/bd_c509.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_0/sim/bd_c509_ila_lib_0.v" \

vlog -work gigantic_mux  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/d322/hdl/gigantic_mux_v1_0_cntr.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_1/bd_c509_g_inst_0_gigantic_mux.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_1/sim/bd_c509_g_inst_0.v" \

vlog -work xlconcat_v2_1_3  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_2/sim/bd_c509_slot_0_aw_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_3/sim/bd_c509_slot_0_w_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_4/sim/bd_c509_slot_0_b_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_5/sim/bd_c509_slot_0_ar_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/bd_0/ip/ip_6/sim/bd_c509_slot_0_r_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_system_ila_0_0/sim/Basic_AXI_Design_system_ila_0_0.v" \
"../../../bd/Basic_AXI_Design/ipshared/4718/src/AXI_WRITE_LOGIC.v" \
"../../../bd/Basic_AXI_Design/ipshared/4718/hdl/mycustomled_v1_0.v" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_mycustomled_0_0/sim/Basic_AXI_Design_mycustomled_0_0.v" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_19  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_4 -93 \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_18  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_20  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ace7/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_xbar_0/sim/Basic_AXI_Design_xbar_0.v" \

vlog -work axi_protocol_converter_v2_1_19  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/ec67/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/8c62/hdl" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ip/Basic_AXI_Design_processing_system7_0_0" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/1b7e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/122e/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/6887/hdl/verilog" "+incdir+../../../../Intro_To_AXI.srcs/sources_1/bd/Basic_AXI_Design/ipshared/9623/hdl/verilog" "+incdir+/opt/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/Basic_AXI_Design/ip/Basic_AXI_Design_auto_pc_0/sim/Basic_AXI_Design_auto_pc_0.v" \
"../../../bd/Basic_AXI_Design/sim/Basic_AXI_Design.v" \

vlog -work xil_defaultlib \
"glbl.v"

