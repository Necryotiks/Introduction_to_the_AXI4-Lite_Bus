/*
 * main.c
 *
 *  Created on: Sep 28, 2019
 *      Author: root
 */

#include "LED_CONTROLLER.h"
int main(void)
{
	uint32_t value = 0xF;
	disableLED(0xF);
	enableLED(0xF);
	while(1)
	{
		value = value ^ 0xF;
		writeLED(value & 0xF);
		for(int i = 0; i < 10000000; i++);
	}

}
