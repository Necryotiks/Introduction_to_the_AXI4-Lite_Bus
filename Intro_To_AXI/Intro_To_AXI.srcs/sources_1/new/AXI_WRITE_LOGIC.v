`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/28/2019 10:03:35 PM
// Design Name: 
// Module Name: AXI_WRITE_LOGIC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AXI_WRITE_LOGIC(
	input wire i_ACLK,
	input wire i_ARSTN,

	input wire [1:0] i_AWADDR,
	input wire i_AWVALID,
	output reg o_AWREADY,

	input wire [31:0] i_WDATA,
	input wire i_WVALID,
	output reg o_WREADY,

	input wire i_BREADY,
	output reg o_BVALID,
	output wire [1:0] o_BRESP,

	//    output wire [31:0] o_DATA_OUT, //ech
	//    output wire [1:0] o_ADDR_OUT,
	// output wire o_DATA_VALID,

	input wire [1:0] i_ARADDR,
	input wire i_ARVALID,
	output reg o_ARREADY,

	output wire [31:0] o_RDATA,
	output wire [1:0] o_RRESP,
	output reg o_RVALID,
	input wire i_RREADY,

	output reg [3:0] o_LED

);
reg [31:0] r_WDATA_LATCH = 32'd0;
reg [1:0] r_WADDR_LATCH = 2'd0;
reg r_WADDR_DONE = 1'd0;
reg r_WDATA_DONE = 1'd0;

reg [31:0] r_RDATA_LATCH = 32'd0;
reg [1:0] r_RADDR_LATCH = 2'd0;
reg r_RADDR_DONE = 1'd0;
reg r_RDATA_DONE = 1'd0;

reg [31:0] r_SLV_REG[3:0];
wire w_WRITE_EN;
wire w_READ_EN;

integer i;
assign o_BRESP = 2'd0;
assign w_WRITE_EN = r_WDATA_DONE & r_WADDR_DONE;
assign w_READ_EN = r_RDATA_DONE & r_RADDR_DONE;
assign o_RRESP = 2'd0;
assign o_RDATA = r_RDATA_LATCH;

initial begin
for(i = 0; i < 4; i = i +1)
	r_SLV_REG[i] = 32'd0;
end
//Write address handshake
always@(posedge i_ACLK)
begin
	if(~i_ARSTN | (i_AWVALID & o_AWREADY))
		o_AWREADY <= 0;
	else if(i_AWVALID & ~o_AWREADY)
		o_AWREADY <= 1;
end
//Write data handshake
always@(posedge i_ACLK)
begin
	if(~i_ARSTN || (i_WVALID & o_WREADY))
		o_WREADY <= 0;
	else if(i_WVALID & ~o_WREADY)
		o_WREADY <= 1;
end

//latching logic
always@(posedge i_ACLK)
begin
	if(~i_ARSTN)
	begin
		r_WDATA_LATCH <= 32'd0;
		r_WADDR_LATCH <= 2'd0;
	end
	else
	begin
		if(i_WVALID & o_WREADY)
			r_WDATA_LATCH <=  i_WDATA;
		if(i_AWVALID & o_AWREADY)
			r_WADDR_LATCH <= i_AWADDR;
	end   
end
//Check if handshake is done
always@(posedge i_ACLK)
begin
	if(~i_ARSTN || (r_WADDR_DONE & r_WDATA_DONE))
	begin
		r_WADDR_DONE <= 0;
		r_WDATA_DONE <= 0;
	end
	else
	begin
		if(i_AWVALID & o_AWREADY)
			r_WADDR_DONE <= 1;
		if(i_WVALID & o_WREADY)
			r_WDATA_DONE <= 1;
	end
end

always@(posedge i_ACLK)
begin
	if(~i_ARSTN || (o_BVALID & i_BREADY))
		o_BVALID <= 0;
	else if(~o_BVALID & (r_WADDR_DONE & r_WDATA_DONE))
		o_BVALID <= 1;

end
//************************************************************************************************
//Read logic
always@(posedge i_ACLK)//FIXME:
begin
	if(~i_ARSTN | (i_ARVALID & o_ARREADY))
		o_ARREADY <= 0;
	else if(i_ARVALID & ~o_ARREADY)
		o_ARREADY <= 1;
end

always@(posedge i_ACLK)
begin
	if(~i_ARSTN || (i_RREADY & o_RVALID))
		o_RVALID <= 0;
	else if(i_RREADY & ~o_RVALID)
		o_RVALID <= 1;
end

//latching logic
always@(posedge i_ACLK)
begin
	if(~i_ARSTN)
	begin
		r_RADDR_LATCH <= 2'd0;
	end
	else
	begin
		if(i_ARVALID & o_ARREADY)
			r_RADDR_LATCH <= i_ARADDR;
	end   
end
//Check if handshake is done
always@(posedge i_ACLK)
begin
	if(~i_ARSTN || (r_RADDR_DONE & r_RDATA_DONE))
	begin
		r_RADDR_DONE <= 0;
		r_RDATA_DONE <= 0;
	end
	else
	begin
		if(i_ARVALID & o_ARREADY)
			r_RADDR_DONE <= 1;
		if(o_RVALID & i_RREADY)
			r_RDATA_DONE <= 1;
	end
end
//TODO: read data
always@(posedge i_ACLK)
begin
	if(~i_ARSTN)
	begin
		for(i = 0; i < 4; i = i +1)
			r_SLV_REG[i] <= 32'd0;
		r_RDATA_LATCH <= 32'd0;//*
	end
	else begin
		if(w_WRITE_EN)
			r_SLV_REG[r_WADDR_LATCH] <=r_WDATA_LATCH;
		if(w_READ_EN)
			r_RDATA_LATCH <= r_SLV_REG[r_RADDR_LATCH];//*
	end
end
//User Logic
always@(posedge i_ACLK)
begin
	o_LED <= r_SLV_REG[0][3:0];
	//o_RGB <= r_SLV_REG[1][6:0];
end


`ifdef FORMAL
reg r_PAST_VALID;
initial begin
	r_PAST_VALID = 0;
	assume(~i_ARSTN);
	assume(~i_AWVALID);
	assume(~i_WVALID);
	assume(~i_ARVALID);
	assume(~i_RVALID);
end
always@(posedge i_ACLK)
begin

	assume(i_ACLK != $past(i_ACLK));
	r_PAST_VALID <= 1;
	if(r_PAST_VALID ) begin
		assume(i_ARSTN);
		if($rose(i_ACLK) && i_ARSTN) begin
			assert(o_BRESP == 0);
			assert(o_RRESP == 0);
			if($past(o_AWREADY) == 1 && $past(i_AWVALID) == 1) begin
				assert(o_AWREADY == 0);
			end
			else if($past(o_AWREADY) == 0 && $past(i_AWVALID) == 1) begin
				assert(o_AWREADY == 1);
			end
			if($past(o_WREADY) == 1 && $past(i_WVALID) == 1) begin
				assert(o_WREADY == 0);
			end
			else if($past(o_WREADY) == 0 && $past(i_WVALID) == 1) begin
				assert(o_WREADY == 1);
			end
			if($past(o_ARREADY) == 1 && $past(i_ARVALID) == 1) begin
				assert(o_ARREADY == 0);
			end
			else if($past(o_ARREADY) == 1 && $past(i_ARVALID) == 1) begin
				assert(o_ARREADY == 1);
			end
			cover(o_LED == 4'hF);//Write transaction sim
			cover(o_RDATA == 32'd7);//Read transaction sim
		end	
	end

end

`endif
endmodule
