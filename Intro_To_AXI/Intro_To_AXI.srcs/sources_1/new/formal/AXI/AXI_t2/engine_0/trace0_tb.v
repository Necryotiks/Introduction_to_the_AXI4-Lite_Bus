`ifndef VERILATOR
module testbench;
  reg [4095:0] vcdfile;
  reg clock;
`else
module testbench(input clock, output reg genclock);
  initial genclock = 1;
`endif
  reg genclock = 1;
  reg [31:0] cycle = 0;
  reg [0:0] PI_i_ACLK;
  reg [0:0] PI_i_WVALID;
  reg [0:0] PI_i_ARVALID;
  reg [0:0] PI_i_AWVALID;
  reg [3:0] PI_i_AWADDR;
  reg [31:0] PI_i_WDATA;
  reg [0:0] PI_i_BREADY;
  reg [3:0] PI_i_ARADDR;
  reg [0:0] PI_i_ARSTN;
  reg [0:0] PI_i_RREADY;
  AXI_WRITE_LOGIC UUT (
    .i_ACLK(PI_i_ACLK),
    .i_WVALID(PI_i_WVALID),
    .i_ARVALID(PI_i_ARVALID),
    .i_AWVALID(PI_i_AWVALID),
    .i_AWADDR(PI_i_AWADDR),
    .i_WDATA(PI_i_WDATA),
    .i_BREADY(PI_i_BREADY),
    .i_ARADDR(PI_i_ARADDR),
    .i_ARSTN(PI_i_ARSTN),
    .i_RREADY(PI_i_RREADY)
  );
`ifndef VERILATOR
  initial begin
    if ($value$plusargs("vcd=%s", vcdfile)) begin
      $dumpfile(vcdfile);
      $dumpvars(0, testbench);
    end
    #5 clock = 0;
    while (genclock) begin
      #5 clock = 0;
      #5 clock = 1;
    end
  end
`endif
  initial begin
`ifndef VERILATOR
    #1;
`endif
    // UUT.$and$AXI_WRITE_LOGIC.v:233$268_Y = 1'b1;
    // UUT.$eq$AXI_WRITE_LOGIC.v:237$275_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:237$276_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:243$283_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:243$284_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:250$291_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:250$292_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:256$299_Y = 1'b0;
    // UUT.$eq$AXI_WRITE_LOGIC.v:256$300_Y = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:234$98_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:234$98_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:238$100_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:238$100_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:241$101_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:241$101_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:244$102_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:244$102_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:247$103_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:247$103_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:251$104_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:251$104_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:254$105_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:254$105_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:257$106_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:257$106_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:260$107_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:260$107_EN = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:263$108_CHECK = 1'b0;
    // UUT.$formal$AXI_WRITE_LOGIC.v:264$109_CHECK = 1'b0;
    // UUT.$past$AXI_WRITE_LOGIC.v:264$52$0 = 32'b00000000000000000000000000000000;
    UUT.o_ARREADY = 1'b0;
    UUT.o_AWREADY = 1'b0;
    UUT.o_BVALID = 1'b0;
    UUT.o_LED = 4'b0000;
    UUT.o_RVALID = 1'b0;
    UUT.o_WREADY = 1'b0;
    UUT.r_PAST_VALID = 1'b0;
    UUT.r_RADDR_DONE = 1'b0;
    UUT.r_RADDR_LATCH = 4'b0000;
    UUT.r_RDATA_DONE = 1'b0;
    UUT.r_RDATA_LATCH = 32'b00000000000000000000000000000000;
    UUT.r_WADDR_DONE = 1'b0;
    UUT.r_WADDR_LATCH = 4'b0000;
    UUT.r_WDATA_DONE = 1'b0;
    UUT.r_WDATA_LATCH = 32'b00000000000000000000000000000000;
    UUT.r_SLV_REG[4'b0000] = 32'b00000000000000000000000000000000;

    // state 0
    PI_i_ACLK = 1'b0;
    PI_i_WVALID = 1'b0;
    PI_i_ARVALID = 1'b0;
    PI_i_AWVALID = 1'b0;
    PI_i_AWADDR = 4'b0000;
    PI_i_WDATA = 32'b00000000000000000000000000000000;
    PI_i_BREADY = 1'b0;
    PI_i_ARADDR = 4'b0000;
    PI_i_ARSTN = 1'b0;
    PI_i_RREADY = 1'b0;
  end
  always @(posedge clock) begin
    // state 1
    if (cycle == 0) begin
      PI_i_ACLK <= 1'b1;
      PI_i_WVALID <= 1'b1;
      PI_i_ARVALID <= 1'b1;
      PI_i_AWVALID <= 1'b1;
      PI_i_AWADDR <= 4'b0000;
      PI_i_WDATA <= 32'b00000000000000000000000000000000;
      PI_i_BREADY <= 1'b0;
      PI_i_ARADDR <= 4'b0000;
      PI_i_ARSTN <= 1'b1;
      PI_i_RREADY <= 1'b1;
    end

    // state 2
    if (cycle == 1) begin
      PI_i_ACLK <= 1'b0;
      PI_i_WVALID <= 1'b1;
      PI_i_ARVALID <= 1'b0;
      PI_i_AWVALID <= 1'b1;
      PI_i_AWADDR <= 4'b0000;
      PI_i_WDATA <= 32'b00000000000000000000000000000111;
      PI_i_BREADY <= 1'b0;
      PI_i_ARADDR <= 4'b1000;
      PI_i_ARSTN <= 1'b1;
      PI_i_RREADY <= 1'b0;
    end

    // state 3
    if (cycle == 2) begin
      PI_i_ACLK <= 1'b1;
      PI_i_WVALID <= 1'b1;
      PI_i_ARVALID <= 1'b1;
      PI_i_AWVALID <= 1'b1;
      PI_i_AWADDR <= 4'b0000;
      PI_i_WDATA <= 32'b00000000000000000000000000001111;
      PI_i_BREADY <= 1'b0;
      PI_i_ARADDR <= 4'b0000;
      PI_i_ARSTN <= 1'b1;
      PI_i_RREADY <= 1'b1;
    end

    // state 4
    if (cycle == 3) begin
      PI_i_ACLK <= 1'b0;
      PI_i_WVALID <= 1'b0;
      PI_i_ARVALID <= 1'b0;
      PI_i_AWVALID <= 1'b0;
      PI_i_AWADDR <= 4'b0000;
      PI_i_WDATA <= 32'b00000000000000000000000000000000;
      PI_i_BREADY <= 1'b0;
      PI_i_ARADDR <= 4'b0000;
      PI_i_ARSTN <= 1'b1;
      PI_i_RREADY <= 1'b0;
    end

    // state 5
    if (cycle == 4) begin
      PI_i_ACLK <= 1'b1;
      PI_i_WVALID <= 1'b0;
      PI_i_ARVALID <= 1'b0;
      PI_i_AWVALID <= 1'b0;
      PI_i_AWADDR <= 4'b0000;
      PI_i_WDATA <= 32'b00000000000000000000000000000000;
      PI_i_BREADY <= 1'b0;
      PI_i_ARADDR <= 4'b0000;
      PI_i_ARSTN <= 1'b1;
      PI_i_RREADY <= 1'b0;
    end

    // state 6
    if (cycle == 5) begin
      PI_i_ACLK <= 1'b0;
      PI_i_WVALID <= 1'b0;
      PI_i_ARVALID <= 1'b0;
      PI_i_AWVALID <= 1'b0;
      PI_i_AWADDR <= 4'b0000;
      PI_i_WDATA <= 32'b00000000000000000000000000000000;
      PI_i_BREADY <= 1'b0;
      PI_i_ARADDR <= 4'b0000;
      PI_i_ARSTN <= 1'b1;
      PI_i_RREADY <= 1'b0;
    end

    genclock <= cycle < 6;
    cycle <= cycle + 1;
  end
endmodule
